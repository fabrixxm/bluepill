"""
Bluepill Engine
"""

import sys

sys.path.append("../")

from bluepill.memory import Memory
from bluepill.singleton import Singleton

STORE_NAME = "BluepillEngine"


class BluepillEngine:
    """
    The Bluepill Engine. Contains BluepillClients
    """

    def __init__(self):
        """
        Initialization of the engine
        """

        self._clients = []  # list of client instances
        self._aktclient = None  # index of actual client

    @property
    def prime_client(self):
        if self._aktclient:
            return self._clients[self._aktclient]

    def save(self):
        """
        Save myself
        """

        Memory().save_object(STORE_NAME, self)


def BluepillEngineFactory(metaclass=Singleton):
    """
    Factory which gets the BluepillEngine
    """

    def __init__(self):
        """
        Initialization
        """

        self._bluepill_engine = None

    def get_engine(self):
        """
        get the Bluepill Engine
        """

        if not self._bluepill_engine:
            self._bluepill_engine = Memory().get_object(STORE_NAME)
            if not self.bluepill_engine:
                self._bluepill_engine = BluepillEngine()

        return self._bluepill_engine
