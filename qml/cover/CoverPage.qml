import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

CoverBackground {
    id: coverback
//    Component.onCompleted: {
//        clienthandler.getRooms()
//    }

    property string lastevent: "none"
    property string roomid: ""
    property string roomname: ""
    property string lroomid: ""
    property var levent
    property int urcount

    Component.onCompleted: {
        clienthandler.getUnreadCount(roomid)
    }

    onStatusChanged: bluepill.covstat = status
    function getText(event) {
        if (event.type === "m.room.message") {
            if (event.content.format) {
                if (event.content.format === 'org.matrix.custom.html') {
                    return bluepill.htmlcss + event.content.formatted_body
                }
            }
            return event.content.body
        } else if (event.type === "m.sticker") {
            return qsTr("[sticker]")
        } else if (event.type === "m.room.encrypted") {
            return qsTr("[encrypted message]")
        } else if (event.type === "m.room.redacted") {
            return qsTr("[message redacted]")
        } else {
            return qsTr("[unknown]")        }
    }

    function renderEvent(room_id, event) {
        // clienthandler.getRoom(room_id)
        if (event.event.content.body) {
            roomid = room_id
            roomname = event.name
            coverback.lastevent = "m.room.event"
            coverlabel.text = getText(event.event)
            writericon.uid = event.user_id
            writericon.roomname = event.name
            writericon.avatarurl = event.avatar_url ? event.avatar_url : ""
            writericon.idirect = true
            writericon.visible = true
        }
    }

    Connections {
        target: clienthandler
        onRoomsList: {
            if (data.length > 0) {
                var room = data[0]
                usericon.uid = room.user_id
                usericon.avatarurl = room.avatar_url ? room.avatar_url : ""
                usericon.idirect = room.direct
                usericon.roomname = room.room_name
                usericon.visible = true
                roomid = room.room_id
                roomname = room.room_name
                if (!levent) {
                    var lastevent = room.last_event
                    coverlabel.text = getText(lastevent.event)
                    writericon.uid = lastevent.user_id
                    writericon.roomname = lastevent.name
                    writericon.avatarurl = lastevent.avatar_url ? lastevent.avatar_url : ""
                    writericon.idirect = true
                    writericon.visible = true
                }
            }
        }

        onMRoomEvent: {
            if (event.type === 'm.room.message') {
                clienthandler.getRoom(roomId)
                console.log(event.event.content.body)
                renderEvent(roomId, event)
                writeAnim.stop()
                if (event.event.content.body) {
                    lroomid = roomId
                    levent = event
                    writeAnim.stop()
                    writerrow.opacity = 1.0
                    messageAnim.start()
                }
            }

        }
        onRoomData: {
            usericon.avatarurl = room.avatar_url ? room.avatar_url : ""
            usericon.uid = room.user_id
            usericon.idirect =  room.direct
            usericon.roomname = room.room_name
            usericon.visible = true
            roomid = room.room_id
            roomname = room.room_name
        }
        onStartTyping: {
            // clienthandler.getRoom(roomId)
            typericon.uid = users[0].user_id
            typericon.roomname = users[0].user_name
            typericon.avatarurl = users[0].image
            typericon.visible = true
            typerlabel.text = qsTr("is typing...")
            writeAnim.start()
        }
        onStopTyping: {
            writeAnim.stop()
            typerrow.opacity = 1.0
            typericon.visible = false
            // clienthandler.getRoom(lroomid)
            typerlabel.text = levent ? renderEvent(lroomid, levent) : ""
        }
        onRoomUnread: {
            if (roomid === roomId) {
                countrec.visible = (count !== 0)
                unread.text = count !== 0 ? count : ""
            }
        }
    }

    Image {
        source: "../../images/matrix_logo.png"
        width: parent.width
        rotation: 315
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        fillMode: Image.PreserveAspectFit
        opacity: 0.1
    }
    Column {
        id: spacer
        height: Theme.iconSizeExtraSmall
        width: parent.width
    }

    Column {
        id: uiconcol
        anchors.top: spacer.bottom
        width: parent.width
        height: Theme.iconSizeLarge
        anchors.margins: Theme.paddingMedium
        z: 1
        UserIcon {
            id: usericon
            anchors.horizontalCenter: parent.horizontalCenter
            width: Theme.iconSizeLarge
            height: Theme.iconSizeLarge
            avatarurl: ""
            roomname: ""
            idirect: false
            uid: ""
            visible: false
            SequentialAnimation {
                id: messageAnim
                running: false
                loops: 2

                NumberAnimation {
                    target: usericon
                    property: "opacity"
                    duration: 1000
                    to: 0.5
                }
                NumberAnimation {
                    target: usericon
                    property: "opacity"
                    duration: 1000
                    to: 1.0
                }
            }
            Rectangle {
                id: countrec
                visible: false
                z: 2
                color: "grey"
                anchors.top: usericon.top
                anchors.right: usericon.right
                width: Theme.itemSizeExtraSmall / 1.5
                height: Theme.itemSizeExtraSmall / 1.5
                radius: width * 0.5
                Label {
                    anchors.centerIn: parent
                    id: unread
                    font.pixelSize: Theme.fontSizeExtraSmall
                    font.bold: true
                    text: ""
                }
            }
        }
    }
   Row {
        id: writerrow
        anchors.top: uiconcol.bottom
        anchors.margins: Theme.paddingMedium
        width: parent.width
        height: parent.height - typerrow.height - usericon.height - Theme.iconSizeMedium
        clip: true

        UserIcon {
            id: writericon
            width: Theme.iconSizeSmall
            height: Theme.iconSizeSmall
            visible: false
            avatarurl: ""
            roomname: ""
            idirect: false
            uid: ""

        }
        Label {
            id: coverlabel
            // anchors.top: usericon.bottom
            text: ""
            width: parent.width - writericon.width
            height: Theme.itemSizeLarge
            wrapMode: Text.WordWrap
            truncationMode: TruncationMode.Fade
            font.pixelSize: Theme.fontSizeTiny
            textFormat: Text.RichText
            padding: Theme.paddingSmall
        }
   }
   Row {
       id: typerrow
       anchors.top: writerrow.bottom
       width: parent.width
       height: Theme.itemSizeLarge
       clip: true
       SequentialAnimation {
           id: writeAnim
           running: false
           loops: 20

           NumberAnimation {
               target: typerrow
               property: "opacity"
               duration: 1000
               to: 0.5
           }
           NumberAnimation {
               target: typerrow
               property: "opacity"
               duration: 1000
               to: 1.0
           }
       }

       UserIcon {
           id: typericon
           width: Theme.iconSizeExtraSmall
           height: Theme.iconSizeExtraSmall
           visible: false
           avatarurl: ""
           roomname: ""
           idirect: false
           uid: ""

       }
       Label {
           id: typerlabel
           // anchors.top: usericon.bottom
           text: ""
           width: parent.width - typericon.width
           height: Theme.itemSizeLarge
           wrapMode: Text.WordWrap
           truncationMode: TruncationMode.Fade
           font.pixelSize: Theme.fontSizeTiny
           textFormat: Text.RichText
           padding: Theme.paddingSmall
       }
  }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-message"
            onTriggered: {
                bluepill.activate()
                if (roomid != "") {
                    pageStack.pop(null, true)
                    pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"), { room_id: roomid, room_name: roomname } )
                }
            }
        }
    }
}
