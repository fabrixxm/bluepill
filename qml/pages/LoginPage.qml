import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: page
    allowedOrientations: Orientation.All

    SilicaFlickable {
        id: loginflick
        anchors.fill: parent
        contentHeight: loginTitle.height + loginTitle.spacing + parameters.height + parameters.spacing

        Component.onCompleted: {
            clienthandler.getHostname()
        }

        Connections {
            target: clienthandler
            onHostName: {
                clientNameField.text = hostname + " - SailfishOS"
            }
            onLoginFailed: {
                console.log("login failed")
                loginButton.logging = false
            }
            onLoggedIn: {
                console.log("logged in")
                pageStack.replace(Qt.resolvedUrl("LogStatusPage.qml"))
            }

//            onLoggedIn: {
//                console.log("Login Ready", token)
//                sessionTokenConf.value = token
//                bluepill.token = token
//                homeServerUrlConf.value = homeServer.text === "" ? "https://matrix.org" : homeServer.text
//                identityServerUrlConf.value = identityServer.text === "" ? "https://vector.im" : identityServer.text
//                userNameConf.value = userField.text
//                userIdConf.value = userId
//                loginButton.logging = false
//                pageStack.replace(Qt.resolvedUrl("DashboardPage.qml"))
//            }
//            onLoginFailed: {
//                console.log("Login Failed")
//                loginButton.logging = false
//            }
        }

        Column {
            id: loginTitle

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Login")
            }
        }


        Column {
            id: parameters
            width: parent.width
            height: children.height
            anchors {
                top: loginTitle.bottom
                left: parent.left
                right: parent.right
                margins: Theme.paddingMedium
            }

            TextField {
                id: userField

                width: parent.width
                height: Theme.itemSizeLarge
                placeholderText: label
                label: qsTr("Username")
                inputMethodHints: Qt.ImhNoAutoUppercase
            }

            PasswordField {
                id: passwordField
                width: parent.width
                height: Theme.itemSizeLarge
                inputMethodHints: Qt.ImhNoAutoUppercase
            }

            TextField {
                id: clientNameField

                width: parent.width
                height: Theme.itemSizeLarge
                placeholderText: label
                label: qsTr("Client Name")
                inputMethodHints: Qt.ImhNoAutoUppercase
                onClicked: systemhandler.getHostname()
            }

            TextSwitch {
                id: customServer
                text: qsTr("Custom Server")
            }

            Label {
                visible: customServer.checked
                text: qsTr("Home server URL")
                font.pixelSize: Theme.fontSizeTiny
            }

            TextField {
                id: homeServer
                visible: customServer.checked

                width: parent.width
                height: Theme.itemSizeLarge
                placeholderText: qsTr("https://matrix.org")
                inputMethodHints: Qt.ImhUrlCharactersOnly
            }

            Label {
                visible: customServer.checked
                text: qsTr("Identity server URL")
                font.pixelSize: Theme.fontSizeTiny
            }

            TextField {
                id: identityServer
                visible: customServer.checked

                width: parent.width
                height: Theme.itemSizeLarge
                placeholderText: qsTr("https://vector.im")
                inputMethodHints: Qt.ImhUrlCharactersOnly
            }

            Button {
                id: loginButton
                property bool logging: false

                anchors.margins: Theme.paddingLarge
                anchors.bottomMargin: Theme.paddingMedium
                text: qsTr("Login")
                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: loginButton
                    running: loginButton.logging
                }

                onClicked: {
                    logging = true
                    var user = userField.text
                    var password = passwordField.text
                    var hostname = clientNameField.text
                    var homeserver = "https://matrix.org"
                    var identserver = "https://vector.im"

                    if (customServer.checked) {
                        if (homeServer.text != "") {
                            homeserver = homeServer.text
                        }
                        if (identityServer.text != "") {
                            identserver = identityServer.text
                        }
                    }

                    clienthandler.doLogin(user, password, hostname, homeserver, identserver)
                }
            }

            Label {
                text: " "
                height: Theme.itemSizeExtraSmall
            }

            Button {
                id: registerButton
                enabled: false
                anchors.margins: Theme.paddingLarge
                text: qsTr("Register")
                onClicked: pageStack.push(Qt.resolvedUrl("RegisterPage.qml"))
            }
        }
    }
}
