import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All


    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        width: parent.width

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr('Enter room')
                onClicked: pageStack.push(Qt.resolvedUrl("JoinRoom.qml"))
            }
            MenuItem {
                text: qsTr('Create room')
            }
            MenuItem {
                text: qsTr('StartChat')
            }
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Preferences")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"))
            }
        }

        Component.onCompleted: {
            clienthandler.getRooms()
        }

        Connections {
            target: clienthandler
            onRoomsList: {
                froomModel.clear()
                proomModel.clear()
                rroomModel.clear()
                lroomModel.clear()
                for (var i = 0; i < data.length; i++) {
                    // console.log('room', data[i].room_name, 'avatar', data[i].avatar_url)
                    if (data[i].prio === 'fav') {
                        froomModel.append(data[i])
                    } else if (data[i].prio === 'low') {
                        lroomModel.append(data[i])
                    } else {
                        if (data[i].direct) {
                            proomModel.append(data[i])
                        } else {
                            rroomModel.append(data[i])
                        }
                    }
                }
                favoritesList.contentWidth = data.length * Theme.iconSizeExtraLarge
            }
            onMRoomEvent: {
                clienthandler.getRooms()
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height + column.spacing + roomlists.height + roomlists.spacing

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.

        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            height: Theme.itemSizeMedium
            PageHeader {
                id: title
                title: qsTr("Rooms")
            }
        }
        Column {
            id: roomlists
            anchors.top: column.bottom
            width: parent.width
            spacing: Theme.paddingMedium
            Item {
                id: favoriteTitle
                // anchors.top: column.bottom
                width: page.width
                height: Theme.itemSizeSmall
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Clicked Favourites")
                    }
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Favourites ") + favoritesList.count
                    anchors.left: parent.left
                    padding: Theme.paddingMedium
                }
                Image {
                    anchors.right: parent.right
                    source: "image://theme/icon-m-right"
                }
            }
            ListView {
                id: favoritesList
                width: parent.width
                height: Theme.itemSizeExtraLarge
                orientation: ListView.HorizontalAndVerticalFlick

                model: ListModel {
                    id: froomModel
                }
                delegate: RoomListItem { }
            }
            Item {
                id: personTitle
                // anchors.top: favoritesList.bottom
                width: page.width
                height: Theme.itemSizeSmall
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Clicked People")
                    }
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("People ") + personsList.count
                    padding: Theme.paddingMedium
                    anchors.left: parent.left
                }
                Image {
                    anchors.right: parent.right
                    source: "image://theme/icon-m-right"
                }
            }
            ListView {
                id: personsList
                // anchors.top: personTitle.bottom
                width: parent.width
                height: Theme.itemSizeExtraLarge
                orientation: ListView.HorizontalAndVerticalFlick

                model: ListModel {
                    id: proomModel
                }
                delegate: RoomListItem { }
            }
            Item {
                id: roomTitle
                // anchors.top: personsList.bottom
                width: page.width
                height: Theme.itemSizeSmall
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Clicked Rooms")
                    }
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Rooms ") + roomsList.count
                    padding: Theme.paddingMedium
                    anchors.left: parent.left
                }
                Image {
                    anchors.right: parent.right
                    source: "image://theme/icon-m-right"
                }
            }
            ListView {
                id: roomsList
                // anchors.top: roomTitle.bottom
                width: parent.width
                height: Theme.itemSizeExtraLarge
                orientation: ListView.HorizontalAndVerticalFlick

                model: ListModel {
                    id: rroomModel
                }
                delegate: RoomListItem { }
            }
            Item {
                id: lowprioTitle
                // anchors.top: roomsList.bottom
                width: page.width
                height: Theme.itemSizeSmall
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Clicked low Priority")
                    }
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Low priority ") + lowprioList.count
                    padding: Theme.paddingMedium
                    anchors.left: parent.left
                }
                Image {
                    anchors.right: parent.right
                    source: "image://theme/icon-m-right"
                }
            }
            ListView {
                id: lowprioList
                // anchors.top: lowprioTitle.bottom
                width: parent.width
                height: Theme.itemSizeExtraLarge
                orientation: ListView.HorizontalAndVerticalFlick

                model: ListModel {
                    id: lroomModel
                }
                delegate: RoomListItem { }
            }
        }

    }
}
